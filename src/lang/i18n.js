import Vue from 'vue'
import VueI18n from 'vue-i18n'
import vnMessage from './vn.json'
import enMessage from './en.json'
import cnMessage from './cn.json'
import jpMessage from './jp.json'
import krMessage from './kr.json'

Vue.use(VueI18n)

const messages = {
  vn: vnMessage,
  en: enMessage,
  cn: cnMessage,
  jp: jpMessage,
  kr: krMessage
}

const i18n = new VueI18n({
  locale: 'en',
  messages,
  fallbackLocale: 'en',
})

export default i18n